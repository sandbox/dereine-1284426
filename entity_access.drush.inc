<?php
/**
 * @file
 * Rebuild entity access with drush.
 */

/**
 * Implements hook_drush_command().
 */
function entity_access_drush_command() {
  $items = array();

  $items['entity-access-rebuild'] = array(
    'description' => "Rebuild entity access",
    'examples' => array(
      'drush entity-access-rebuild',
    ),
    'aliases' => array('ea-rebuild'),
  );

  return $items;
}

/**
 * Drush command callback.
 */
function drush_taxonomy_entity_access_rebuild() {
  $entity_info = entity_get_info();
  $operations = array();

  foreach($entity_info as $type => $info) {
    if (entity_access_entity_type_access_enabled($type)) {
      $operations[] = array('entity_access_rebuild_entity_access', array($type));
    }
  }

  $batch = array(
    'operations' => $operations,
  );

  batch_set($batch);

  // Process the batch.
  $batch =& batch_get();
  $batch['progressive'] = FALSE;
  batch_process();
}
