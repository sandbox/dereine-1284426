<?php

/**
 * @file
 * A generic entity access api similar to entity_access api.
 */

/**
 * Fetch an array of entity types with entity access.
 *
 * @return array
 */
function entity_access_get_info() {
  $access_info = &drupal_static(__FUNCTION__);
  if (!isset($access_info)) {
    $access_info = drupal_map_assoc(module_invoke_all('entity_access_info'));
    drupal_alter('entity_access_get_info', $access_info);
  }
  return $access_info;
}

/**
 * Check if entity access is enabled for a single entity type
 *
 * @param $entity_type
 *   The given entity type.
 * @return
 *   TRUE if entity access is enabled, FALSE otherwise.
 */
function entity_access_entity_type_access_enabled($entity_type) {
  $access_info = entity_access_get_info();
  return isset($access_info[$entity_type]) && count(module_implements('entity_access_grants'));
}

/**
 * Implements hook_entity_insert().
 */
function entity_access_entity_insert($entity, $entity_type) {
  entity_access_acquire_grants($entity_type, $entity);
}

/**
 * Implements hook_entity_update().
 */
function entity_access_entity_update($entity, $entity_type) {
  entity_access_acquire_grants($entity_type, $entity);
}

/**
 * Ignore this php "bug".
 */
class EntityAccessQueryExtenderHelper extends SelectQueryExtender {
  function __construct() { }

  public function getQuery($object) {
    return $object->query;
  }
}

/**
 * Implements hook_query_alter().
 */
function entity_access_query_alter(QueryAlterableInterface $query) {
  $tags = array();
  if ($query instanceof SelectQuery) {
    $tags = $query->alterTags;
  }
  // Awesome!!!!!!
  // @see http://www.phpwtf.org/php-protected-and-assault
  elseif ($query instanceof SelectQueryExtender) {
    $object = new EntityAccessQueryExtenderHelper();
    if (isset($object->getQuery($query)->alterTags)) {
      $tags = $object->getQuery($query)->alterTags;
    }
  }
  if (count($tags)) {
    foreach (entity_access_get_info() as $entity_type) {
      if (isset($tags[$entity_type . '_access'])) {
        _entity_access_query_entity_access_access_alter($entity_type, $query, 'entity');
      }
    }
  }
}

/**
 * Fetch an array of permission IDs granted to the given user ID.
 *
 * The implementation here provides only the universal "all" grant. A node
 * access module should implement hook_node_grants() to provide a grant
 * list for the user.
 *
 * After the default grants have been loaded, we allow modules to alter
 * the grants array by reference. This hook allows for complex business
 * logic to be applied when integrating multiple node access modules.
 *
 * @param $entity_type
 *
 * @param $op
 *   The operation that the user is trying to perform.
 * @param $account
 *   The user object for the user performing the operation. If omitted, the
 *   current user is used.
 * @return
 *   An associative array in which the keys are realms, and the values are
 *   arrays of grants for those realms.
 */
function entity_access_grants($entity_type, $op, $account = NULL) {

  if (!isset($account)) {
    $account = $GLOBALS['user'];
  }

  // Fetch node access grants from other modules.
  $grants = module_invoke_all('entity_access_grants', $entity_type, $account, $op);
  // Allow modules to alter the assigned grants.
  drupal_alter('entity_access_grants', $entity_type, $grants, $account, $op);

  return array_merge(array('all' => array(0)), $grants);
}

/**
 * Determines whether the user has a global viewing grant for all entities.
 *
 * Checks to see whether any module grants global 'view' access to a user
 * account; global 'view' access is encoded in the {entity_access} table as a
 * grant with nid=0. If no entity access modules are enabled, entity.module defines
 * such a global 'view' access grant.
 *
 * This function is called when a entity listing query is tagged with
 * 'entity_access'; when this function returns TRUE, no entity access joins are
 * added to the query.
 *
 * @param $entity_type
 *
 * @param $account
 *   The user object for the user whose access is being checked. If omitted,
 *   the current user is used.
 *
 * @return
 *   TRUE if 'view' access to all nodes is granted, FALSE otherwise.
 *
 * @see hook_entity_access_grants()
 * @see _entity_query_entity_access_alter()
 */
function entity_access_view_all_entities($entity_type, $account = NULL) {
  global $user;
  if (!$account) {
    $account = $user;
  }

  // Statically cache results in an array keyed by $account->uid.
  $access = &drupal_static(__FUNCTION__);
  if (isset($access[$account->uid])) {
    return $access[$account->uid];
  }

  // If no modules implement the node access system, access is always TRUE.
  if (!module_implements('entity_access_grants')) {
    $access[$account->uid] = TRUE;
  }
  else {
    $query = db_select('entity_access');
    $query->addExpression('COUNT(*)');
    $query
      ->condition('entity_id', 0)
      ->condition('entity_type', $entity_type)
      ->condition('grant_view', 1, '>=');

    $grants = db_or();
    foreach (entity_access_grants($entity_type, 'view', $account) as $realm => $gids) {
      foreach ($gids as $gid) {
        $grants->condition(db_and()
          ->condition('gid', $gid)
          ->condition('realm', $realm)
        );
      }
    }
    if (count($grants) > 0 ) {
      $query->condition($grants);
    }
    $access[$account->uid] = $query
      ->execute()
      ->fetchField();
  }

  return $access[$account->uid];
}


/**
 * Implements hook_query_TAG_alter().
 *
 * This is the hook_query_alter() for queries tagged with 'entity_access'.
 * It adds entityeaccess checks for the user account given by the 'account'
 * meta-data (or global $user if not provided), for an operation given by
 * the 'op' meta-data (or 'view' if not provided; other possible values are
 * 'update' and 'delete').
 */
function entity_access_query_entity_access_alter(QueryAlterableInterface $query) {
  foreach (entity_access_get_info() as $entity_type) {
    _entity_access_query_entity_access_access_alter($entity_type, $query, 'entity');
  }
}

/**
 * Implements hook_query_TAG_alter().
 *
 * This function implements the same functionality as
 * entity_access_query_entity_access_alter() for the SQL field storage engine. Entity access
 * conditions are added for field values belonging to nodes only.
 */
function entity_access_query_entity_field_access_alter(QueryAlterableInterface $query) {
  foreach (entity_access_get_info() as $entity_type) {
    _entity_access_query_entity_access_access_alter($entity_type, $query, 'field');
  }
}

/**
 * Helper for entity access functions.
 *
 * @param $query
 *   The query to add conditions to.
 * @param $type
 *   Either 'entity' or 'field' depending on what sort of query it is. See
 *   entity_access_query_entity_access_alter() and entity_access_query_entity_field_access_alter()
 *   for more.
 */
function _entity_access_query_entity_access_access_alter($entity_type, $query, $type) {
  global $user;

  // Base table for this entity.
  $entity_info = entity_get_info($entity_type);
  $entity_id_key = $entity_info['entity keys']['id'];

  // Read meta-data from query, if provided.
  if (!$account = $query->getMetaData('account')) {
    $account = $user;
  }
  if (!$op = $query->getMetaData('op')) {
    $op = 'view';
  }

  // If $account can bypass node access, or there are no node access modules,
  // or the operation is 'view' and the $acount has a global view grant (i.e.,
  // a view grant for node ID 0), we don't need to alter the query.

  if (user_access('bypass entity access', $account)) {
    return;
  }
  if (!entity_access_entity_type_access_enabled($entity_type)) {
    return;
  }
  if ($op == 'view' && entity_access_view_all_entities($entity_type, $account)) {
    return;
  }

  $tables = $query->getTables();
  $base_table = $query->getMetaData('base_table');

  // If no base table is specified explicitly, search for one.
  if (!$base_table) {
    $fallback = '';
    foreach ($tables as $alias => $table_info) {
      if (!($table_info instanceof SelectQueryInterface)) {
        $table = $table_info['table'];
        // If the entity type base table is in the query, it wins immediately.
        if ($table == $entity_info['base table']) {
          $base_table = $table;
          break;
        }
        // Check whether the table has a foreign key to node.nid. If it does,
        // do not run this check again as we found a base table and only node
        // can triumph that.
        if (!$base_table) {
          // The schema is cached.
          $schema = drupal_get_schema($table);
          if (isset($schema['fields']['nid'])) {
            if (isset($schema['foreign keys'])) {
              foreach ($schema['foreign keys'] as $relation) {
                if ($relation['table'] === $entity['base table'] && $relation['columns'] === array($entity_info['entity keys']['id'] => $entity_info['entity keys']['id'])) {
                  $base_table = $table;
                }
              }
            }
            else {
              // At least it's a nid. A table with a field called nid is very
              // very likely to be a node.nid in a node access query.
              $fallback = $table;
            }
          }
        }
      }
    }
    // If there is nothing else, use the fallback.
    if (!$base_table) {
      if ($fallback) {
        watchdog('security', 'Your entity listing query is using @fallback as a base table in a query tagged for entity access. This might not be secure and might not even work. Specify foreign keys in your schema to entity.entity_id ', array('@fallback' => $fallback), WATCHDOG_WARNING);
        $base_table = $fallback;
      }
      else {
        throw new Exception(t('Query tagged for entity access but there is no nid. Add foreign keys to node.nid in schema to fix.'));
      }
    }
  }

  // Prevent duplicate records.
  $query->distinct();

  // Find all instances of the base table being joined -- could appear
  // more than once in the query, and could be aliased. Join each one to
  // the node_access table.

  $grants = entity_access_grants($entity_type, $op, $account);
  if ($type == 'field') {
    // The original query looked something like:
    // @code
    //  SELECT nid FROM sometable s
    //  INNER JOIN node_access na ON na.nid = s.nid
    //  WHERE ($node_access_conditions)
    // @endcode
    //
    // Our query will look like:
    // @code
    //  SELECT entity_type, entity_id
    //  FROM field_data_something s
    //  LEFT JOIN entity_access ea ON s.entity_id = ea.entity_id AND ea.entity_type = 'node'
    //  WHERE (entity_type = 'node' AND $node_access_conditions) OR (entity_type <> 'node')
    // @endcode
    //
    // So instead of directly adding to the query object, we need to collect
    // in a separate db_and() object and then at the end add it to the query.
    $entity_conditions = db_and();
  }
  foreach ($tables as $ealias => $tableinfo) {
    $table = $tableinfo['table'];
    if (!($table instanceof SelectQueryInterface) && $table == $base_table) {
      // The node_access table has the access grants for any given node so JOIN
      // it to the table containing the nid which can be either the node
      // table or a field value table.
      if ($type == 'entity') {
        $access_alias = $query->join('entity_access', 'ea', '%alias.entity_id = ' . $ealias . '.' . $entity_id_key);
      }
      else {
        $access_alias = $query->leftJoin('entity_access', 'ea', '%alias.' . $entity_id_key . ' = ' . $ealias . '.entity_id');
        $base_alias = $ealias;
      }

      $grant_conditions = db_or();
      // If any grant exists for the specified user, then user has access
      // to the node for the specified operation.
      foreach ($grants as $realm => $gids) {
        foreach ($gids as $gid) {
          $grant_conditions->condition(db_and()
            ->condition($access_alias . '.gid', $gid)
            ->condition($access_alias . '.realm', $realm)
          );
        }
      }

      $count = count($grant_conditions->conditions());
      if ($type == 'entity') {
        if ($count) {
          $query->condition($grant_conditions);
        }
        $query->condition($access_alias . '.grant_' . $op, 1, '>=');
      }
      else {
        if ($count) {
          $entity_conditions->condition($grant_conditions);
        }
        $entity_conditions->condition($access_alias . '.grant_' . $op, 1, '>=');
      }
    }
  }

  if ($type == 'field' && count($entity_conditions->conditions())) {
    // All the node access conditions are only for field values belonging to
    // nodes.
    $entity_conditions->condition("$base_alias.entity_type", $entity_type);
    $or = db_or();
    $or->condition($entity_conditions);
    // If the field value belongs to a non-node entity type then this function
    // does not do anything with it.
    $or->condition("$base_alias.entity_type", $entity_type, '<>');
    // Add the compiled set of rules to the query.
    $query->condition($or);
  }
}

/**
 * Gets the list of node access grants and writes them to the database.
 *
 * This function is called when a node is saved, and can also be called by
 * modules if something other than a node save causes node access permissions to
 * change. It collects all node access grants for the node from
 * hook_node_access_records() implementations, allows these grants to be altered
 * via hook_node_access_records_alter() implementations, and saves the collected
 * and altered grants to the database.
 *
 * @param $entity_type
 *
 *
 * @param $entity
 *   The $entity to acquire grants for.
 *
 * @param $delete
 *   Whether to delete existing node access records before inserting new ones.
 *   Defaults to TRUE.
 */
function entity_access_acquire_grants($entity_type, $entity, $delete = TRUE) {
  $grants = module_invoke_all('entity_access_records', $entity_type, $entity);
  // Let modules alter the grants.
  drupal_alter('entity_access_records', $grants, $entity_type, $entity);
  // If no grants are set and the node is published, then use the default grant.
  if (empty($grants) && !empty($entity->status)) {
    $grants[] = array('realm' => 'all', 'gid' => 0, 'grant_view' => 1, 'grant_update' => 0, 'grant_delete' => 0);
  }
  else {
    // Retain grants by highest priority.
    $grant_by_priority = array();
    foreach ($grants as $g) {
      $grant_by_priority[intval($g['priority'])][] = $g;
    }
    krsort($grant_by_priority);
    $grants = array_shift($grant_by_priority);
  }

  entity_access_write_grants($entity_type, $entity, $grants, NULL, $delete);
}

/**
 * Writes a list of grants to the database, deleting any previously saved ones.
 *
 * If a realm is provided, it will only delete grants from that realm, but it
 * will always delete a grant from the 'all' realm. Modules that utilize
 * node_access can use this function when doing mass updates due to widespread
 * permission changes.
 *
 * Note: Don't call this function directly from a contributed module. Call
 * entity_access_acquire_grants() instead.
 *
 * @param $entity
 *   The $node being written to. All that is necessary is that it contains a
 *   nid.
 * @param $grants
 *   A list of grants to write. Each grant is an array that must contain the
 *   following keys: realm, gid, grant_view, grant_update, grant_delete.
 *   The realm is specified by a particular module; the gid is as well, and
 *   is a module-defined id to define grant privileges. each grant_* field
 *   is a boolean value.
 * @param $realm
 *   If provided, only read/write grants for that realm.
 * @param $delete
 *   If false, do not delete records. This is only for optimization purposes,
 *   and assumes the caller has already performed a mass delete of some form.
 */
function entity_access_write_grants($entity_type, $entity, $grants, $realm = NULL, $delete = TRUE) {
  $entity_info = entity_get_info($entity_type);
  $entity_access_info = entity_access_get_info();

  if ($delete) {
    $query = db_delete('entity_access')
      ->condition('entity_type', $entity_type)
      ->condition('entity_id', $entity->{$entity_info['entity keys']['id']});
    if ($realm) {
      $query->condition('realm', array($realm, 'all'), 'IN');
    }
    $query->execute();
  }

  // Only perform work when [entity_type]_access modules are active.
  if (!empty($grants) && isset($entity_access_info[$entity_type])) {
    $query = db_insert('entity_access')->fields(array('entity_type', 'entity_id', 'realm', 'gid', 'grant_view', 'grant_update', 'grant_delete'));
    foreach ($grants as $grant) {
      $grant['entity_type'] = $entity_type;
      if ($realm && $realm != $grant['realm']) {
        continue;
      }
      // Only write grants; denies are implicit.
      if ($grant['grant_view'] || $grant['grant_update'] || $grant['grant_delete']) {
        $grant['entity_id'] = $entity->{$entity_info['entity keys']['id']};
        $query->values($grant);
      }
    }
    $query->execute();
  }
}

/**
 * Implements hook_permission().
 */
function entity_access_permission() {
  return array(
    'bypass entity access' => array(
      'title' => t('Bypass entity access control'),
      'description' => t('View, edit and delete all entities regardless of permission restrictions.'),
      'restrict access' => TRUE,
    ),
  );
}

/**
 * Rebuild entity access for a given entity type.
 *
 * @param $entity_type
 */
function entity_access_rebuild_entity_access($entity_type) {
  $entity_info = entity_get_info($entity_type);

  $ids = db_select($entity_info['base table'])
    ->fields($entity_info['base table'], array($entity_info['entity keys']['id']))
    ->execute()
    ->fetchCol();

  foreach ($ids as $id) {
    $entity = entity_load($entity_type, array($id), array(), TRUE);
    // To preserve database integrity, only acquire grants if the node
    // loads successfully.
    if (!empty($entity)) {
      $entity = reset($entity);
      entity_access_acquire_grants($entity_type, $entity);
    }
  }
}
