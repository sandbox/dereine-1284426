<?php

/**
 * Implements hook_entity_access_info().
 *
 * Define which entity types can be used with entity_access. If you implement
 * a general entity_access module return all entity types you support.
 */
function hook_entity_access_info() {
  return array('entity_type');
}

/**
 * Implements hook_entity_access_info_alter().
 */
function hook_entity_access_info_alter(&$entity_access) { }

/**
 * Implements hook_entity_access_records().
 *
 * Define all access records as in hook_node_access_records.
 *
 * @see hook_node_access_records
 */
function hook_entity_access_records($entity_type, $entity) {
    if ($entity_type != 'entity_test') {
    return;
  }

  $grants = array();
  $grants[] = array(
    'realm' => 'entity_access_test',
    'gid' => 1234,
    'grant_view' => 1,
    'grant_update' => 0,
    'grant_delete' => 0,
    'priority' => 0,
  );
  $grants[] = array(
    'realm' => 'entity_access_test_author',
    'gid' => $entity->uid,
    'grant_view' => 1,
    'grant_update' => 1,
    'grant_delete' => 1,
    'priority' => 0,
  );

  return $grants;
}


/**
 * Implements hook_entity_access_grants().
 */
function hook_entity_access_grants($entity_type, $account, $op) {
  $grants = array();

  return $grants;
}
